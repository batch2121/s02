package com.zuitt.batch212;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ArraySample {
    public static void main(String[] args) {
        int intArray[] = {1,2,3,120,12,10};
        Arrays.sort(intArray);
//        System.out.println(Arrays.toString(intArray));

        String stringArray[] = new String[] {"Johnro", "Rohann", "Alvina"};
//        System.out.println(Arrays.toString(stringArray));
//        int index = Arrays.binarySearch(stringArray, "pogi");
//        System.out.println(index);
        
//        Multidimensional Array
        String[][] classroom = new String[][] {{"1", "2", "3"},{"1", "2", "3"},{"1", "2", "3"}};

//        System.out.println(Arrays.deepToString(classroom));

        ArrayList<String> students = new ArrayList<>(Arrays.asList("123","123123123"));

        students.add("Johnro");

//        System.out.println(students);
//        System.out.println(students.get(0));

        students.set(0, "Rohann");
//        System.out.println(students.size());
//        System.out.println(students);
        students.remove(0);
//        System.out.println(students);

        HashMap<String, String> employeeRole = new HashMap<>();

        employeeRole.put("Captain", "Luffy");
        employeeRole.put("Dead", "Brook");

//        System.out.println(employeeRole);
        System.out.println(employeeRole.get("Captain"));

        System.out.println(employeeRole.remove("Dead"));
//        System.out.println(employeeRole);

        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();




    }
}

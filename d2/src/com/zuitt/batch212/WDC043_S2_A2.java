package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class WDC043_S2_A2 {
    public static void main(String[] args) {
        int[] prime_numbers = new int[]{ 2, 3, 5, 7, 11};

        System.out.println("The first prime number is " + prime_numbers[0]);
        System.out.println("The second prime number is " + prime_numbers[1]);
        System.out.println("The third prime number is " + prime_numbers[2]);
        System.out.println("The fourth prime number is " + prime_numbers[3]);
        System.out.println("The fifth prime number is " + prime_numbers[4]);

        ArrayList<String> names = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        System.out.println("My friends are: " + names);

        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("Toothpaste", 15);
        inventory.put("Toothbrush", 20);
        inventory.put("Soap", 12);

        System.out.println("Our current inventory consist of: "+ inventory);
    }
}
